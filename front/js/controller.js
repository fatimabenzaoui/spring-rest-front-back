document.getElementById("addMessage").addEventListener('submit', saveMessage);

this.init();

function init() {
    fetch("http://localhost:8080/api/msg")
    .then(Response => Response.json())
    .then(data => {
        let output = "";
        data.forEach(msg => {
            output += `
            <tr>
                <td>${msg.id}</td>
                <td>${msg.author}</td>
                <td>${msg.messages}</td>
            </tr>
            `;
        });
        document.getElementById("msgTable").innerHTML = output;
    });
}

function saveMessage(event) {
    event.preventDefault();
    let author = document.getElementById("author").value;
    let messages = document.getElementById("messages").value;

    fetch("http://localhost:8080/api/msg", {
        method:"POST",
        headers: {"Accept":"application/json, text/plain, */*", "Content-type":"application/json"},

        body:JSON.stringify({author:author, messages:messages})
    })
    .then(Response => Response.json());
    location.reload();
};
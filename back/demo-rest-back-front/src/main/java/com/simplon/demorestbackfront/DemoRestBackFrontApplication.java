package com.simplon.demorestbackfront;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoRestBackFrontApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoRestBackFrontApplication.class, args);
	}

}

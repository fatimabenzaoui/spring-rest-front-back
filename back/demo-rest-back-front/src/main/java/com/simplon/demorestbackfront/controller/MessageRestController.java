package com.simplon.demorestbackfront.controller;

import java.util.List;

import com.simplon.demorestbackfront.dao.MessageDAO;
import com.simplon.demorestbackfront.model.Message;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin("*")
@RestController
public class MessageRestController {

    @Autowired
    private MessageDAO messageDAO;

    @GetMapping("/api/msg")
    public List<Message> messageList() {
        return messageDAO.findAll();  
    }

    @PostMapping("/api/msg")
    public void save(@RequestBody Message message) {
        messageDAO.save(message);
    }
}

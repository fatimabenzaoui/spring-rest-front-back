package com.simplon.demorestbackfront.dao;

import com.simplon.demorestbackfront.model.Message;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MessageDAO extends JpaRepository<Message, Integer> {
    
}
